

.. _diffraction_section_label:


*********************************************************************
Diffraction : Huygens-Fresnel principle and Fraunhofer Approximation
*********************************************************************

Let us consider the optical system on the following figure :numref:`setup_label`. 

.. _exp_setup_label:

.. figure:: _static/diffraction_montage_4f.png
	:name: setup_label

	Schematic representation of the experimental setup
	
..  :align: center

A monochromatic point source :math:`S_0` of wavelength :math:`\lambda` is located at the focal length :math:`f_1` of a first lens (:math:`L_1`), this lens generates a plane 
wave :math:`\underline{\Psi_{0}^{-}}` (parallel to Oz) from the incident spherical wave.
This plane wave goes towards the pierced screen (D) and the diffraction phenomenon occurs at long distance because only a small part of the plane wave passes through the screen.
The second lens (L2) makes it possible to observe this interference phenomenon at infinity by reducing the Fourier space to a finite distance corresponding to the focal length :math:`f_2` of 
(L2) (where we put an observation screen (E)). 

Throughout this part, we will develop the mathematical formalism used to describe the phenomenon of diffraction (Huygens-Fresnel principle, transmittance definition, Fraunhofer approximation, etc).  


Huygens-Fresnel principle
**************************

The Huygens-Fresnel principle is based on the wave behavior of light. For the diffraction phenomenon, this principle says that each point of an aperture illuminated by a plane 
wave becomes a point source. 
All these points emit spherical waves which will interfere with each other.
Mathematically, that means the wave function :math:`\Psi(P)` at the position :math:`P=(x_{\text{i}},y_{\text{i}},z_{\text{i}})` on the observation screen (E) is defined by

.. math::
    \Psi(P)=
	\int_{\Sigma}\underline{\Psi_{0}^{+}}(M)\frac{e^{ikr}}{r}\mathrm{d}\Sigma ,

where *M=(x,y, 0)* is an arbitrary position on the perforated screen, :math:`r=\vert\vert\mathbf{MP}\vert\vert=\sqrt{(x-x_{i})^{2}+(y-y_{i})^{2}+z_{i}^{2}}` the norm of the vector :math:`\mathbf{MP}` 
(see :numref:`fraunhofer_label`), :math:`k=\frac{2\pi}{\lambda}` the wave number of the light, :math:`\underline{\Psi_{0}^{+}}(M)` is the wave function just 
after the perforated screen (see :numref:`setup_label`) and :math:`\mathrm{d}\Sigma` is the surface of this screen.


.. _fraunh_label:

.. figure:: _static/diffraction_fraunhofer_schema.png
	:name: fraunhofer_label
	
	Schematic representation of the propagation angles of light between the perforated screen (D) and the observation screen (E)
	
..  :align: center


Fraunhofer approximation
**************************

The Fraunhofer diffraction consists in viewing the diffraction pattern at a long distance from the diffracting object. 
Thus we can make the Taylor series of *r* 

.. math::
	r\simeq r_{i}-\frac{xx_{i}+yy_{i}}{r_{i}}\simeq r_{i}-(\alpha x+\beta y)

with :math:`r_{i}=\sqrt{x_{i}^{2}+y_{i}^{2}+z_{i}^{2}}`, :math:`\alpha=\dfrac{x_{i}}{r_{i}}` and :math:`\beta = \dfrac{y_{i}}{r_{i}}`.

By replacing the approximate expression of *r* in :math:`\Psi(P)`, we obtain

.. math::
	\Psi(P)\simeq\int_{\Sigma}\underline{\Psi_{0}^{+}}(x,y)\frac{e^{ikr_{i}}}{r_{i}}e^{-i\mathbf{k}\cdot\mathbf{OM}}\mathrm{d}\Sigma

with :math:`\mathbf{k}=\frac{2\pi}{\lambda}\frac{\mathbf{OP}}{OP}`.


This expression is valid only for a perfectly flat screen (D). 
If its surface :math:`\mathrm{d}\Sigma` is not flat we must introduce a factor *Q*, depending of the angle between the vector :math:`\mathbf{MP}` 
and the normal to :math:`\mathrm{d}\Sigma` in *M*, such as

.. math::
	\Psi(P)\simeq Q\int_{\Sigma}\underline{\Psi_{0}^{+}}(x,y)\frac{e^{ikr_{i}}}{r_{i}}e^{-i\mathbf{k}\cdot\mathbf{OM}}\mathrm{d}\Sigma.

	
The last expression is the fondamental relation of the diffraction in the Fraunhofer approximation.
	
The integrals are performed on the variables `(x, y)`, thus the term :math:`Q\dfrac{e^{ikr_{i}}}{r_{i}}` can be considered as a constant.
In other words, all the diffraction information is contained under the integral and we can reduce the previous expression to

.. math::
    \Psi(P)\simeq \int_{\Sigma}\underline{\Psi_{0}^{+}}(x,y)e^{-i\mathbf{k}\cdot\mathbf{OM}}\mathrm{d}\Sigma .


Spatial frequency wavenumber
*****************************	
  	
We introduce the well-know nomenclature of spatial frequency wave numbers `(u,v)` as

.. math::
	u = \frac{\alpha}{\lambda}\quad\text{and}\quad v = \frac{\beta}{\lambda}.

We define previously 

.. math::
	\alpha=\frac{x_{i}}{r_{i}}\quad\text{and}\quad\beta = \frac{y_{i}}{r_{i}},

thus using the sine definition we easily observe that :math:`\alpha=\sin\theta_{x}` and :math:`\beta=\sin\theta_{y}` with :math:`\theta_{x}` and :math:`\theta_{y}` 
the angles of propagation represented on the figure :numref:`fraunhofer_label`.
	

Consequently, the relation between the spatial frequency wave numbers and the angles :math:`\theta_{x}` and :math:`\theta_{y}` are

.. math::
	\alpha=\frac{\sin\theta_{x}}{\lambda}\quad\text{and}\quad\beta = \frac{\sin\theta_{y}}{\lambda}.

These two relations mean that for a point P on the observation screen, a single pair of angles, and only one, are associated with this point. 
So, only light beams with corresponding angles will interfere in P.	


If we introduce *u* and *v* in :math:`\Psi(P)` we obtain

.. math::
    \Psi(P) =
	 \Psi(u,v) =
	 \int_{\Sigma}\underline{\Psi_{0}^{+}}(x,y)e^{-i2\pi(u x+v y)}\mathrm{d}\Sigma.

	 
	 
Transmittance function
*****************************

For a plane wave perpendicular to the diaphragm (D) (propagating along Oz), we have got a simple relation between the wave function :math:`\underline{\Psi_{0}^{+}}` directly after the screen 
and the incident wave function :math:`\underline{\Psi_{0}^{-}}`.
They are related by the transmittance `t(x,y)` as

.. math::
    \underline{\Psi_{0}^{+}} =
		t(x,y)\underline{\Psi_{0}^{-}}.

This function is equal to 1 if *M* is part of the opening of (D) and equal to 0 otherwise.

In that conditions (normal incident plane wave), we have got

.. math::
	\Psi(u,v)\simeq\underline{\Psi_{0}^{-}}\int_{\Sigma}t(x,y)e^{-i2\pi(u x+v y)}\mathrm{d}\Sigma.
	
Since :math:`I(u,v)=\vert\Psi(u,v)\vert^2` :

.. math::
	I(u,v)\simeq\vert\int_{\Sigma}t(x,y)e^{-i2\pi(u x+v y)}\mathrm{d}\Sigma\vert^2,

thus, if we omit the coefficient :math:`\dfrac{Q^2}{r_i^2}`, the intensity is proportional to the Fourier transform of *t(x,y)*.

Two examples of transmittance function are given in the section :ref:`rectangular_section_label` and :ref:`double_slit_section_label`.
	

