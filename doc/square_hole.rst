
.. _rectangular_section_label:

***********************************
Rectangular Aperture
***********************************


Theoretical formalism
**************************

As a reminder of the section :ref:`diffraction_section_label`, the general expression of the amplitude at the position P in the Fourier plane is 

.. math::
    \Psi(P) =
	 \Psi(u,v) =
	 \int_{\Sigma}t(x,y)\underline{\Psi_{0}^{-}}e^{-i2\pi(u x+v y)}\mathrm{d}\Sigma

with `t(x,y)` the transmittance function, `(u,v)` the spatial frequency wave numbers, :math:`\underline{\Psi_{0}^{-}}` the plane wave amplitude just before 
the holed screen and :math:`\mathrm{d}\Sigma` is the area of this screen.


For a rectangular aperture the transmittance function `t(x,y)` is defined as the product of rectangular functions

.. math::
	t(x,y) = 
	\text{rect}(\frac{x}{a})\text{rect}(\frac{y}{b})

where *a* is the dimension of the aperture along the Ox direction and *b* along Oy.

This definition determines the upper and lower integration limits for the both integrals along the *x* and *y* directions.

After having calculated the two Fourier transforms, we find the amplitude 

.. math::
	\Psi(u,v) =
	\underline{\Psi_{0}^{-}}ab\, \text{sinc}(ua)\text{sinc}(vb)

with :math:`u=\frac{x_\text{i}}{\lambda f}` and :math:`v=\frac{y_\text{i}}{\lambda f}`.

Therefore, the intensity is equal to

.. math::
	I(u,v) = 
	\vert \Psi(u,v) \vert^2 =
	I_0\, \text{sinc}^2(ua)\text{sinc}^2(vb)
	
where :math:`I_0=\vert\underline{\Psi_{0}^{-}}\vert^2 a^2 b^2`.	
			  
If `a=b`, the interfringe on the diffraction pattern is equal to :math:`i=\frac{\lambda f}{a}=\frac{\lambda f}{b}` in both Ox and Oy directions.
Now if we lengthen the opening in one direction, for example if we have :math:`b=100a`, the interfringe is reduced by a factor 100 in the same direction. 
In addition, if the laser wavelength or the focal distance increases, the size of the diffraction pattern increases too.

Thus, the interest of the <https://gitlab.com/diffraction_qt/diffraction_qt/-/blob/master/diffraction/diffraction_simple_square_hole.py>`_ file is to study these different
effects.


Graphic interface description
=========================================

For the rectangular aperture shape, the graphic interface is divided into four parts (see :numref:`graphic_label`).

.. _interf_label:

.. figure:: _static/inter_graph_square_hole.png
	:name: graphic_label
	
	Image of the graphic interface
	
..  :align: center

Zone (a) contains all variables that can be modified by the user, (b) is a 2D map of the aperture qualitatively showing the aspect ratio between *a* and *b*, 
(c) represents the 2D diffraction pattern map corresponding to the aperture and the graph on (d) represents the normalized intensity along the Oy=0 axis.
Curve (d) gives additional informations over (c) such as the values of the normalized intensity (dashed black lines) of the two highest peaks and the full width 
at half maximum of the main peak (dashed red line).
The axes of the map on the figure (c) are in meters like the horizontal axis on figure (d).
Before to give some examples, we will describe the variables area. 



Variables area
=========================================

.. _variable_label:
.. figure:: _static/variables_area.png
	
	Zoom on the "Variables" window
	
.. :align: center

Explanation of each parameter:

* **a** : dimension of the aperture along Ox (in micrometers)
* **b** : dimension of the aperture along Oy (in micrometers)
* **lamda** : laser wavelength (in nanometers)
* **focal** : lens focal length (in centimeters)
* **High Contrast box** : increase the contrast of the 2D diffraction pattern map by reducing the upper limit of the color bar from 1 to 0.02
* **Plot button** : starts the calculation

Effect of a, b, lamda and focal
********************************
The following two images show the evolution of the aperture and the diffraction pattern map if we have :math:`a=b=55 \mu \text{m}` and :math:`a=55 \mu \text{m}` and :math:`b=110 \mu \text{m}`. 
In the second case, the opening is lengthened by a factor 2 along the direction Oy and the interfringe is contracted by this same factor.


.. _effect_a_b_label:

.. figure:: _static/aperture_function_a_b.png

	Examples of apertures for two pairs of (a,b)

.. :align: center


.. _effect_a_b_2D_map_label:

.. figure:: _static/diffraction_pattern_function_a_b.png

	Examples of 2D diffraction maps for two pairs of (a,b)

.. :align: center

As said previously, an increase in the laser wavelength or the lens focal length induce an increase in the interfringe, so it is like a zoom on the 2D diffraction map 
(see :numref:`diffraction_lamda_label`).
We have made calculations with :math:`\lambda = 400 \text{nm}` (blue in visible spectrum) and :math:`\lambda = 800 \text{nm}` (red in visible spectrum).

.. _effect_lamda_label:

.. figure:: _static/effet_lamda_diffraction_map.png
	:name: diffraction_lamda_label
	
	Effect of the laser wavelength on the diffraction pattern
	
..	:align: center
	
	

High contrast box
**********************
The **High contrast** check box allows to increase the contrast on the diffraction 2d map by modifying the upper color bar limit (see :numref:`high_contrast_label`).


.. _effect_hight_contrast_label:

.. figure:: _static/high_contrast_effect.png
	:name: high_contrast_label
	
	Effect of the **High contrast** check box on the diffraction pattern
	
..  :align: center