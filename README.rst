***********************************
Requirements / Installation
***********************************

diffraction_qt is available on `gitlab <https://gitlab.com/diffraction_qt/diffraction_qt>`_. 

Detailed documentation with many examples is avaiable at the `diffraction_qt documentation website <https://clementmajorel.gitlab.io/diffraction_qt-doc/>`_




Requirements
================================

Python
------------------
    - **numpy** (`numpy <http://www.numpy.org/>`_)
    - **PyQt5** (`PyQt5 <http://pypi.org/project/PyQt5/>`_)
    - **matplotlib** (`matplotlib <http://matplotlib.org/>`_)


Line command to run the main python file script in the terminal
    - **python3 script.py**

Python files finishing by '_ui.py' contains the graphic interfaces class and must not be run.
These files are imported in the corresponding main files. They must be in the same directory to be import or change the import line :
"import script_ui as ui"
by
"import directory\script_ui as ui"

Authors
=========================
   - C\. Majorel


