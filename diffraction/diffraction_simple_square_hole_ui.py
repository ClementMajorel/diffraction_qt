# -*- coding: utf-8 -*-
from __future__ import print_function, division

# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QApplication

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(1337, 967)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
        self.verticalLayout.setContentsMargins(0, -1, -1, -1)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.verticalLayout_plot = QtWidgets.QVBoxLayout()
        self.verticalLayout_plot.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
        self.verticalLayout_plot.setContentsMargins(0, -1, -1, -1)
        self.verticalLayout_plot.setObjectName(_fromUtf8("verticalLayout_plot"))
        self.variablesGroupBox = QtWidgets.QGroupBox(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.variablesGroupBox.sizePolicy().hasHeightForWidth())
        self.variablesGroupBox.setSizePolicy(sizePolicy)
        self.variablesGroupBox.setMinimumSize(QtCore.QSize(600, 180))
        self.variablesGroupBox.setObjectName(_fromUtf8("variablesGroupBox"))
        self.variablesGroupBox.setSizePolicy(QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Minimum)
        self.numberA = QtWidgets.QLabel(self.variablesGroupBox)
        self.numberA.setGeometry(QtCore.QRect(20, 30, 53, 16))
        self.numberA.setObjectName(_fromUtf8("numberA"))
        self.numberB = QtWidgets.QLabel(self.variablesGroupBox)
        self.numberB.setGeometry(QtCore.QRect(20, 70, 53, 16))
        self.numberB.setObjectName(_fromUtf8("numberB"))
        self.numberlamda = QtWidgets.QLabel(self.variablesGroupBox)
        self.numberlamda.setGeometry(QtCore.QRect(200, 30, 80, 16))
        self.numberlamda.setObjectName(_fromUtf8("numberlamda"))
        self.numberfocal = QtWidgets.QLabel(self.variablesGroupBox)
        self.numberfocal.setGeometry(QtCore.QRect(200, 70, 80, 16))
        self.numberfocal.setObjectName(_fromUtf8("numberfocal"))
        self.numberA_SpinBox = QtWidgets.QDoubleSpinBox(self.variablesGroupBox)
        self.numberA_SpinBox.setGeometry(QtCore.QRect(50, 30, 70, 22))
        self.numberA_SpinBox.setMinimum(10)
        self.numberA_SpinBox.setMaximum(500)
        self.numberA_SpinBox.setObjectName(_fromUtf8("numberASpinBox"))
        self.numberA_SpinBox.setValue(110)
        self.numberB_SpinBox = QtWidgets.QDoubleSpinBox(self.variablesGroupBox)
        self.numberB_SpinBox.setGeometry(QtCore.QRect(50, 70, 70, 22))
        self.numberB_SpinBox.setMinimum(10)
        self.numberB_SpinBox.setMaximum(500)
        self.numberB_SpinBox.setObjectName(_fromUtf8("numberBSpinBox"))
        self.numberB_SpinBox.setValue(110)
        self.numberlamda_SpinBox = QtWidgets.QDoubleSpinBox(self.variablesGroupBox)
        self.numberlamda_SpinBox.setGeometry(QtCore.QRect(250, 30, 70, 22))
        self.numberlamda_SpinBox.setMinimum(400)
        self.numberlamda_SpinBox.setMaximum(800)
        self.numberlamda_SpinBox.setObjectName(_fromUtf8("numberlamdaSpinBox"))
        self.numberlamda_SpinBox.setValue(540)
        self.numberfocal_SpinBox = QtWidgets.QDoubleSpinBox(self.variablesGroupBox)
        self.numberfocal_SpinBox.setGeometry(QtCore.QRect(250, 70, 70, 22))
        self.numberfocal_SpinBox.setMinimum(10)
        self.numberfocal_SpinBox.setMaximum(50)
        self.numberfocal_SpinBox.setObjectName(_fromUtf8("numberfocalSpinBox"))
        self.numberfocal_SpinBox.setValue(20)
        self.label_6 = QtWidgets.QLabel(self.variablesGroupBox)
        self.label_6.setGeometry(QtCore.QRect(130, 30, 21, 16))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.label_7 = QtWidgets.QLabel(self.variablesGroupBox)
        self.label_7.setGeometry(QtCore.QRect(130, 70, 21, 16))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.label_8 = QtWidgets.QLabel(self.variablesGroupBox)
        self.label_8.setGeometry(QtCore.QRect(330, 30, 21, 16))
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.label_9 = QtWidgets.QLabel(self.variablesGroupBox)
        self.label_9.setGeometry(QtCore.QRect(330, 70, 21, 16))
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.plotButton = QtWidgets.QPushButton(self.variablesGroupBox)
        font = QtGui.QFont()

        self.plotButton.setText("Plot")
        self.plotButton.setGeometry(QtCore.QRect(10, 250, 360, 50))
        self.verticalLayout.addWidget(self.variablesGroupBox)
        
        self.checkBoxContrast = QtWidgets.QCheckBox(self.variablesGroupBox)
        self.checkBoxContrast.setText('High Contrast')
        self.checkBoxContrast.setGeometry(QtCore.QRect(30, 200, 150, 35))
        
        fig = Figure()
        self.axes = fig.add_subplot(111)
        self.axes.set_title("Figure diffractive hole")
        self.trouCurveMatplotlibwidget = FigureCanvas(fig)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.trouCurveMatplotlibwidget.sizePolicy().hasHeightForWidth())
        self.trouCurveMatplotlibwidget.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setKerning(True)
        self.trouCurveMatplotlibwidget.setFont(font)
        self.trouCurveMatplotlibwidget.setObjectName(_fromUtf8("trouCurveMatplotlibwidget"))
        self.verticalLayout.addWidget(self.trouCurveMatplotlibwidget)
        
        
        self.horizontalLayout_2.addLayout(self.verticalLayout)
        fig3 = Figure()
        self.axes3 = fig3.add_subplot(111)
        self.axes3.set_title("Diffraction pattern")
        self.diffCurveMatplotlibwidget = FigureCanvas(fig3)
        self.diffCurveMatplotlibwidget.setObjectName(_fromUtf8("diffCurveMatplotlibwidget"))
        self.diffCurveMatplotlibwidget.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_plot.addWidget(self.diffCurveMatplotlibwidget)
        fig2 = Figure()
        self.axes2 = fig2.add_subplot(111)
        self.axes2.set_title("Diffraction pattern curve along Oy=0")
        self.lineCurveMatplotlibwidget = FigureCanvas(fig2)
        self.lineCurveMatplotlibwidget.setObjectName(_fromUtf8("lineCurveMatplotlibwidget"))
        self.lineCurveMatplotlibwidget.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_plot.addWidget(self.lineCurveMatplotlibwidget)
        
        self.horizontalLayout_2.addLayout(self.verticalLayout_plot)
        self.horizontalLayout.addLayout(self.horizontalLayout_2)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1337, 26))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.variablesGroupBox.setTitle(_translate("MainWindow", "Variables", None))
        self.numberA.setText(_translate("MainWindow", "a", None))
        self.numberB.setText(_translate("MainWindow", "b", None))
        self.numberlamda.setText(_translate("MainWindow", "lamda", None))
        self.numberfocal.setText(_translate("MainWindow", "focal", None))
        self.label_6.setText(_translate("MainWindow", "µm", None))
        self.label_7.setText(_translate("MainWindow", "µm", None))
        self.label_8.setText(_translate("MainWindow", "nm", None))
        self.label_9.setText(_translate("MainWindow", "cm", None))

if __name__ == "__main__":
    import sys
    app = QApplication(sys.argv)
    MainWindow = QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())