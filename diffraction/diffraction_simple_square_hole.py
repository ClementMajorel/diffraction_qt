# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 17:10:51 2020
Part of the software where things are executed. We link the graphic part with
the functions of calculation scripts here.
@author: majorel
"""
from __future__ import print_function, division

import diffraction_simple_square_hole_ui as ui

import numpy as np

from scipy.signal import argrelextrema as argr_e

from PyQt5.QtWidgets import QMainWindow, QApplication

class Exploit_Results(ui.Ui_MainWindow):
    """ Main class with the graphic aspect of the exploitResults_ui script.
    Gives life to the buttons by attributing functions to them """
    def __init__(self):
        """ Call the window, connect functions """
        app = QApplication(sys.argv)
        MainWindow = QMainWindow()
        self.setupUi(MainWindow)
        MainWindow.show()
        self.plot_condition = False
        self.a = self.numberA_SpinBox.value()
        self.b = self.numberB_SpinBox.value()
        self.lamda = self.numberlamda_SpinBox.value()
        self.focal = self.numberfocal_SpinBox.value()
        self.set_values()
        # Following objects are lists so we have one for each main tab
        # Link physical objects (buttons, ..) with actions
        self.connect_function()

        # Edit Plot
        sys.exit(app.exec_())

    def connect_function(self):
        """ Connects events of the window items with actions """
        # Load files buttons
        self.plotButton.clicked.connect(self.plot_trou_diffractif)
        self.plotButton.clicked.connect(self.plot_figure_diffraction)
        self.plotButton.clicked.connect(self.plot_line_diffraction)
        self.checkBoxContrast.clicked.connect(self.Check_plot)
        self.numberA_SpinBox.valueChanged.connect(self.set_values)
        self.numberB_SpinBox.valueChanged.connect(self.set_values)
        self.numberlamda_SpinBox.valueChanged.connect(self.set_values)
        self.numberfocal_SpinBox.valueChanged.connect(self.set_values)

    def set_values(self):
        """set values of variables used from what is put in software"""
        self.a = self.numberA_SpinBox.value()*1e-6
        self.b = self.numberB_SpinBox.value()*1e-6
        self.lamda = self.numberlamda_SpinBox.value()*1e-9
        self.focal = self.numberfocal_SpinBox.value()*1e-2

    
    def pos(self, x_min, x_max, y_min, y_max, NX, NY):
        X,Y = np.meshgrid(np.linspace(x_min,x_max,NX),np.linspace(y_min,y_max,NY))
        MAP = np.array([X.T, Y.T]).T
        return MAP
        ## --- function plotting the aperture shape
    def trou(self):
    ## window size of the pierced screen
        x_min = -5.*self.a ; y_min = -5.*self.b       
        x_max = 5.*self.a ; y_max = 5.*self.b
    ## conditions for the same window dimensions along Ox and Oy   
        if self.a>self.b:
            x_min+=5*(self.a-self.b); x_max-=5*(self.a-self.b)
        elif self.b>self.a:
            y_min+=5*(self.b-self.a); y_max-=5*(self.b-self.a)
        
        NX = NY = 250
        self.val_map=[]
        for xi in np.linspace(x_min,x_max,NX):
            for yi in np.linspace(y_min,y_max,NY):
                if abs(xi)<self.a/2. and abs(yi)<self.b/2.:
                    self.val_map.append(1)               ## if val_map=1 aperture else screen
                else :
                    self.val_map.append(0)
        self.val_map = np.array(np.reshape(self.val_map, (NX, NY)))
        return self.val_map
    
        ## --- Intensity function (https://en.wikipedia.org/wiki/Fraunhofer_diffraction_equation)
    def Intensite(self, MAP):
        u = MAP.T[0]/self.lamda/self.focal
        v = MAP.T[1]/self.lamda/self.focal
        I = np.sinc(u*self.a)**2*np.sinc(v*self.b)**2
        return I
    
    
    def Check_plot(self):
        if self.plot_condition:
            self.plot_figure_diffraction()
    
    def plot_trou_diffractif(self):
        """ Plot a 2D view of the aperture.
            """
        
        self.trouCurveMatplotlibwidget.figure.axes[0].clear()
        
        trou_diff = self.trou().T
        
        self.trouCurveMatplotlibwidget.figure.axes[0].imshow(trou_diff, cmap = 'jet', aspect='equal')
        self.trouCurveMatplotlibwidget.figure.axes[0].set_title("Figure diffractive hole")
        self.trouCurveMatplotlibwidget.figure.axes[0].set_xlabel("X axis")
        self.trouCurveMatplotlibwidget.figure.axes[0].set_ylabel("Y axis")
        self.trouCurveMatplotlibwidget.figure.axes[0].set_xticks([])
        self.trouCurveMatplotlibwidget.figure.axes[0].set_yticks([])
        self.trouCurveMatplotlibwidget.draw()



    def plot_figure_diffraction(self):
        """ Plot diffraction pattern in the Fourier plane.
            """
        self.plot_condition = True
        self.diffCurveMatplotlibwidget.figure.axes[0].clear()
        
        NX = NY = 2001                   ## -- discretization size in the Fourier plane
        self.x_min = self.y_min = -5e-3          ## -- min values along Ox and Oy on the Fourier plane
        self.x_max = self.y_max = 5e-3          ## -- max values along Ox and Oy on the Fourier plane
        
        ## --- set of ([x, y]) positions where we compute the intensity
        self.positions = self.pos(self.x_min, self.x_max, self.y_min, self.y_max, NX, NY)
        
        
        self.I = self.Intensite(self.positions)
        
        X = np.reshape(self.I, (NX, NY)).T

        self.diffCurveMatplotlibwidget.figure.axes[0].set_title("Diffraction pattern")
        self.diffCurveMatplotlibwidget.figure.axes[0].hlines(0., self.x_min, self.x_max, color='C0', linestyles='dashed')
        self.diffCurveMatplotlibwidget.figure.axes[0].set_xlabel("X position (m)")
        self.diffCurveMatplotlibwidget.figure.axes[0].set_ylabel("Y position (m)")
        if self.checkBoxContrast.isChecked():
            im = self.diffCurveMatplotlibwidget.figure.axes[0].imshow(X, 
                                                                      aspect='equal', cmap='hot', 
                                                                      vmin=np.min(X), vmax=np.max(X)-0.98*np.max(X), 
                                                                      extent=[self.x_min,self.x_max,self.y_min,self.y_max])
            cb = self.diffCurveMatplotlibwidget.figure.colorbar(im, ticks=[0, 0.02])
        else :
            im = self.diffCurveMatplotlibwidget.figure.axes[0].imshow(X, 
                                                                      aspect='equal', cmap='hot', 
                                                                      vmin=np.min(X), vmax=np.max(X), 
                                                                      extent=[self.x_min,self.x_max,self.y_min,self.y_max])
            cb = self.diffCurveMatplotlibwidget.figure.colorbar(im, ticks=[0, 1])
        cb.set_label("Normalized intensity")
        self.diffCurveMatplotlibwidget.draw()
        cb.remove()

    def plot_line_diffraction(self):
        """ Plot diffraction pattern curve along the Oy=0 axis.
            """
        
        self.lineCurveMatplotlibwidget.figure.axes[0].clear()
        
        ## --- extraction of I along Oy=0 axis
        indx = np.where(self.positions.T[1]==0)
        x_line = self.positions.T[0][indx]
        I_line = self.I[indx]
        
        ## --- index of local extrema
        indx_max = argr_e(I_line, np.greater)
        I_max = I_line[indx_max]    ## -- corresponding value of the intensity
        
        ## --- index of the two main peaks (plot the horizontal line on the graph)
        idx_hlines = indx_max[0][np.where(I_max>0.03)]
        
        ## -- initialization of the list of specific y-ticks (plot config)
        yticks = []
        yticks.append(0)
        yticks.append(0.5)
        yticks.extend(I_max[I_max>0.03][:-1])
        
        ## --- index full width at half maximum of main peak using symmetrical properties of I_line
        idx1_fwhm = (np.abs(I_line[:-int(len(I_line)/2)]-0.5)).argmin()         ## idx first half part of the window
        idx2_fwhm = (np.abs(I_line[-int(len(I_line)/2):]-0.5)).argmin()+int(len(I_line)/2) ## idx second half part of the window
        
        ## -- position and length of the red dashed line on the curve
        x = x_line[idx1_fwhm]
        y = I_line[idx1_fwhm]
        dx = x_line[idx2_fwhm]-x_line[idx1_fwhm]
        
        ## --- checking that the main peak is not too wide to allow the plot of the horizontal lines
        self.lineCurveMatplotlibwidget.figure.axes[0].plot(x_line, I_line)
        if len(indx_max[0])>1:
            self.lineCurveMatplotlibwidget.figure.axes[0].hlines(I_max[I_max>0.03][0], x_line[0], x_line[idx_hlines[0]], color='k', linestyles='dashed')
            self.lineCurveMatplotlibwidget.figure.axes[0].hlines(I_max[I_max>0.03][1], x_line[0], x_line[idx_hlines[1]], color='k', linestyles='dashed')
            self.lineCurveMatplotlibwidget.figure.axes[0].hlines(y, x, x+dx, color='r', linestyles='dashed')
            self.lineCurveMatplotlibwidget.figure.axes[0].hlines(y, x, x+dx, color='r', linestyles='dashed')
            self.lineCurveMatplotlibwidget.figure.axes[0].text(x+dx+0.0001, y+0.01, 'fwhm='+str(np.round(dx,5))+ 'm', color='r')
        self.lineCurveMatplotlibwidget.figure.axes[0].set_title("Diffraction pattern curve along Oy=0")
        self.lineCurveMatplotlibwidget.figure.axes[0].set_xlabel("X position (m)")
        self.lineCurveMatplotlibwidget.figure.axes[0].set_xlim(left=self.x_min, right=self.x_max)
        self.lineCurveMatplotlibwidget.figure.axes[0].set_ylabel("Normalized intensity")
        self.lineCurveMatplotlibwidget.figure.axes[0].set_yticks(yticks)
        self.lineCurveMatplotlibwidget.draw()

if __name__ == "__main__":
    import sys
    Exploit_Results()